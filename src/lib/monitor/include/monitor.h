/**
 *****************************************************************************
 * @title   monitor.h
 * @author  Rainer Becker
 * @date    02.Apr. 2013
 * @brief   The console will  be used for testing the software and hardware
 *******************************************************************************/

#ifndef __MONITOR_H__
#define __MONITOR_H__

#include <stdbool.h>
#include <task.h>

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief start Monitor task
 * @retval none
 */
bool monitor_init();

/**
 * @brief Will be used as instruction processor, the over uart received buffer will be analyzed and executed
 * @param buf the input buffer from uart as char stream with blank as delimiter
 * @param pos the number of received characters
 * @retval none
 * @see  switch_cpu_burner() read_data() write_data() last_error() set_trigger_conf() monitor_mram_test()
 * @see sram_dump() print_all_commands() run_tolerance_check() monitor_flash_test() run_adc_test() illu_temp_read() log_msg()
 */
bool monitor_analyse_cmd(char *buf, size_t buflen);



#ifdef __cplusplus
}
#endif

#endif /* __CONSOLE_H__ */
