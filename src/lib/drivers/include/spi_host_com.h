/*
 * spi_host_com.h
 *
 *  Created on: 09.09.2013
 *      Author: debeckri
 */

#ifndef SPI_HOST_COM_H_
#define SPI_HOST_COM_H_


void spi_host_com_activate_request_port();

void spi_host_com_set_imx_request_active();
void spi_host_com_set_imx_request_passive();

#endif /* SPI_HOST_COM_H_ */
